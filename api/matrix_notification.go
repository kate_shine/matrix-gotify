package api

type MatrixNotificationPayload struct {
	Notification MatrixNotification `json:"notification"`
}

type MatrixNotification struct {
	EventId string `json:"event_id"`
	RoomId string `json:"room_id"`
	Type string `json:"type"`
	Sender string `json:"sender"`
	SenderDisplayName string `json:"sender_display_name"`
	RoomName string `json:"room_name"`
	RoomAlias string `json:"room_alias"`
	UserIsTarget bool `json:"user_is_target"`
	Prio string `json:"prio"`
	Content MatrixContent `json:"content"`
	Counts MatrixCounts `json:"counts"`
	Devices []MatrixDevice `json:"devices"`
}

type MatrixContent struct {
	Msgtype string `json:"msgtype"`
	Body string `json:"body"`
}

type MatrixCounts struct {
	Unread int64 `json:"undread"`
	MissedCalls int64 `json:"missed_calls"`
}

type MatrixDevice struct {
	AppId string `json:"app_id"`
	Pushkey string `json:"pushkey"`
	PushkeyTs int64 `json:"pushkey_ts"`
}
